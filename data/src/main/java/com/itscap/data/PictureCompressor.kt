package com.itscap.data

import android.content.Context
import android.graphics.Bitmap
import kotlinx.coroutines.Dispatchers
import me.shouheng.compress.Compress
import me.shouheng.compress.strategy.Strategies
import me.shouheng.compress.strategy.config.ScaleMode
import java.nio.ByteBuffer

class PictureCompressor {

    suspend fun compressAndScalePicture(
        context: Context,
        bitmap: Bitmap,
        quality: Int,
        maxHeight: Float,
        maxWidth: Float
    ): Bitmap? {
        return Compress
            .with(context, bitmap)
            .setQuality(quality)
            .strategy(Strategies.compressor())
            .setMaxHeight(maxHeight)
            .setMaxWidth(maxWidth)
            .setScaleMode(ScaleMode.SCALE_SMALLER)
            .asBitmap()
            .get(Dispatchers.IO)
    }

    fun convertBitmapToByteArray(bitmap: Bitmap): ByteArray {
        val byteBuffer = ByteBuffer.allocate(bitmap.byteCount)
        bitmap.copyPixelsToBuffer(byteBuffer)
        byteBuffer.rewind()
        return byteBuffer.array()
    }
}