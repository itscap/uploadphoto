package com.itscap.data

import android.Manifest
import android.content.ClipData
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.ContextCompat


class PictureAcquisitionHelper {

    companion object {
        const val REQ_COD_INTENT_REQ_STORAGE_PERMISSIONS = 3000
        const val REQ_COD_INTENT_CHOOSE_IMAGES = 3001
    }

    private fun getStoragePermissionArr() = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private fun getChoosePicturesFromGalleryIntent() =
        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            .apply { putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true); }

    private fun resolveImage(contentResolver: ContentResolver, imagesUri: Uri): Bitmap? {
        var outputFile: Bitmap? = null
        contentResolver.acquireContentProviderClient(imagesUri)?.let { providerClient ->
            providerClient.openFile(imagesUri, "r")?.let { parcelFileDescriptor ->
                outputFile = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.fileDescriptor)
                parcelFileDescriptor.close()
            }
            providerClient.close()
        }
        return outputFile
    }

    fun hasStoragePermissions(context: Context): Boolean {
        val hasUnacceptedPermission = getStoragePermissionArr()
            .map { perm -> ContextCompat.checkSelfPermission(context, perm) }
            .contains(PackageManager.PERMISSION_DENIED)
        return !hasUnacceptedPermission
    }

    fun requireStoragePermissions(activity: AppCompatActivity) {
        ActivityCompat.requestPermissions(
            activity,
            getStoragePermissionArr(),
            REQ_COD_INTENT_REQ_STORAGE_PERMISSIONS
        )
    }

    fun startRetrievePicturesIntent(activity: AppCompatActivity) {

        startActivityForResult(
            activity,
            getChoosePicturesFromGalleryIntent(),
            REQ_COD_INTENT_CHOOSE_IMAGES,
            null
        )
    }

    suspend fun getBitmapListFromActivityResults(
        contentResolver: ContentResolver,
        clipData: ClipData
    ): List<Bitmap> {
        val imagesList = mutableListOf<Bitmap>()

        var i = 0
        while (i < clipData.itemCount) {
            val uri = clipData.getItemAt(i).uri
            resolveImage(contentResolver, uri)?.let { bitmap ->
                imagesList.add(bitmap)
            }
            ++i
        }

        return imagesList
    }

}