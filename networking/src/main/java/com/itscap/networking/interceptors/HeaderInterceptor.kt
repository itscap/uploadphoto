package com.itscap.networking.interceptors

import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class HeaderInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val authenticatedRequest: Request = request
            .newBuilder()
            .headers(getHeaders())
            .build()
        return chain.proceed(authenticatedRequest)
    }

    private fun getHeaders(): Headers {
        val builder: Headers.Builder = Headers.Builder()
        .add(
            "user-agent",
            "PRINTUP_ANDROID/9.9.9 (Software/PRINTUP_ANDROID;+http://corporate.photosi.com/)"
        )
        .add(
            "x-api-key",
            "AIzaSyCccmdkjGe_9Yt-INL2rCJTNgoS4CXsRDc"
        )
        return builder.build()
    }
}
