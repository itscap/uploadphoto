package com.itscap.networking

import com.itscap.networking.data.AddUserConfigReq
import com.itscap.networking.data.AddUserConfigResp
import com.itscap.networking.data.CreateUserConfigReq
import com.itscap.networking.data.CreateUserConfigResp
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*

interface NetworkApi {

    @POST("configurators/consumer/prints/configurations")
    @Headers("Content-Type: application/json" )
    suspend fun createUserConfiguration(
        @Body createUserConfigReq: CreateUserConfigReq
    ): CreateUserConfigResp


    @POST("configurators/consumer/prints/files")
    @Headers("Content-Type: application/json")
    suspend fun addUserConfiguration(
        @Body addUserConfigReq: AddUserConfigReq
    ): AddUserConfigResp

    @PUT
    suspend fun uploadImage(@Url uploadUrl: String,@Body requestBody : RequestBody):ResponseBody
}