package com.itscap.networking

import com.itscap.networking.interceptors.HeaderInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetworkProvider {

    private const val BASE_URL = "https://api.photoforse.online/"
    private const val CONNECTION_TIMEOUT_SEC = 20L
    private const val READ_TIMEOUT_SEC = 30L

    private lateinit var httpClient: OkHttpClient
    lateinit var api: NetworkApi

    fun setup() {

        httpClient = OkHttpClient()
            .newBuilder()
            .connectTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT_SEC, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .addInterceptor(HeaderInterceptor())
            .build()

        api = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .build()
            .create(NetworkApi::class.java)
    }
}