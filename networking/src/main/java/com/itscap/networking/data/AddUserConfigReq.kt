package com.itscap.networking.data

data class AddUserConfigReq(
    var configurationId: String? = null,
    var quantity: Int? = null,
    var type: String? = null
)