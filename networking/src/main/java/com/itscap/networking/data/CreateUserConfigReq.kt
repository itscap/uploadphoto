package com.itscap.networking.data

data class CreateUserConfigReq(
    var category: String? = null,
    var correction: String? = null,
    var format: String? = null,
    var paper: String? = null,
    var passepartout: String? = null,
    var photoText: String? = null,
    var quality: String? = null,
    var quantity: Int? = null
)