package com.itscap.networking.data

import android.content.Context
import com.itscap.networking.R

class ErrorResponse() {

    companion object {

        const val ERROR_CODE_GENERIC = 1000L
        const val ERROR_CODE_FORBIDDEN = 1001L
        const val ERROR_CODE_CONNECTION = 1002L
        const val ERROR_CODE_SERVER_ISSUE = 1003L
        const val ERROR_CODE_TOKEN_EXPIRED = 1004L
        const val ERROR_CODE_STORAGE_PERM = 2000L
        const val ERROR_CODE_RETRIEVE_PICTURES = 2001L
        const val ERROR_CODE_COMPRESS_PICTURE = 2002L
        const val ERROR_CODE_CREATE_USR_CONFIG = 2003L
        const val ERROR_CODE_ADD_USR_CONFIG = 2004L
        const val ERROR_CODE_UPLOAD_PICTURE = 2005L
    }

    var httpErrorCode: Int = -1
    var mappedErrorCode: Long = -1 // Errors mapped in-app

    constructor(mappedErrorCode: Long) : this() {
        this.mappedErrorCode = mappedErrorCode
    }

    fun asGeneric(): ErrorResponse {
        mappedErrorCode = ERROR_CODE_GENERIC
        return this
    }

    fun asForbidden(): ErrorResponse {
        mappedErrorCode = ERROR_CODE_FORBIDDEN
        return this
    }

    fun asConnection(): ErrorResponse {
        mappedErrorCode = ERROR_CODE_CONNECTION
        return this
    }

    fun asTokenExpired(): ErrorResponse {
        mappedErrorCode = ERROR_CODE_TOKEN_EXPIRED
        return this
    }

    fun withHttpErrorCode(httpErrorCode: Int): ErrorResponse {
        this.httpErrorCode = httpErrorCode
        return this
    }

    fun overrideErrorCode(mappedErrorCode: Long): ErrorResponse {
        this.mappedErrorCode = mappedErrorCode
        return this
    }

    fun getMessage(context: Context?) = getLocalString(context, mappedErrorCode)

    private fun getLocalString(context: Context?, code: Long?): String {
        var message: String? = null
        context?.let {
            message = when (code) {
                ERROR_CODE_GENERIC -> it.getString(R.string.error_generic)
                ERROR_CODE_STORAGE_PERM -> it.getString(R.string.error_storage_permissions)
                ERROR_CODE_RETRIEVE_PICTURES -> it.getString(R.string.error_retrieve_pictures)
                ERROR_CODE_COMPRESS_PICTURE,
                ERROR_CODE_CREATE_USR_CONFIG,
                ERROR_CODE_ADD_USR_CONFIG,
                ERROR_CODE_UPLOAD_PICTURE -> it.getString(R.string.error_upload_pictures)
                else -> it.getString(R.string.error_generic)
            }
        }
        return message ?: "Spiacente c'è stato un problema."
    }

}