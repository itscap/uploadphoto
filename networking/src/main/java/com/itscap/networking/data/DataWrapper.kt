package com.itscap.networking.data

sealed class DataWrapper<out T> {
    data class Success<out T>(val data: T) : DataWrapper<T>()
    data class Error(val code: Int? = null, val error: ErrorResponse? = null) :
        DataWrapper<Nothing>()
}