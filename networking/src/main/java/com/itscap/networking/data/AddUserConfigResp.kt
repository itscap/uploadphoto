package com.itscap.networking.data

data class AddUserConfigResp(
    var configurationId: String? = null,
    var filename: Any? = null,
    var id: String? = null,
    var localFileUri: Any? = null,
    var quantity: Int? = null,
    var type: String? = null,
    var uploadUrl: String? = null,
    var url: String? = null
)