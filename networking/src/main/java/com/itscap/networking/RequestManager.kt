package com.itscap.networking

import com.itscap.networking.data.*
import com.itscap.networking.data.DataWrapper.Error
import com.itscap.networking.data.DataWrapper.Success
import okhttp3.RequestBody
import okhttp3.ResponseBody

class RequestManager {

    private suspend fun createUserConfiguration(): DataWrapper<CreateUserConfigResp> {
        val req = CreateUserConfigReq(
            category = "SMALL_SIZE",
            paper = "GLOSSY",
            passepartout = "NO",
            format = "10x10",
            quality = "NORMAL",
            correction = "AUTO",
            photoText = "NO",
            quantity = 1
        )
        return RequestHandler()
            .apiCall { NetworkProvider.api.createUserConfiguration(req) }
            .also { resp ->
                if (resp is Error) {
                    resp.apply { error?.overrideErrorCode(ErrorResponse.ERROR_CODE_CREATE_USR_CONFIG) }
                }
            }
    }

    private suspend fun addUserConfiguration(configId: String): DataWrapper<AddUserConfigResp> {
        val req = AddUserConfigReq(
            configurationId = configId,
            quantity = 1,
            type = "SinglePageProductImage"
        )
        return RequestHandler()
            .apiCall { NetworkProvider.api.addUserConfiguration(req) }
            .also { resp ->
                if (resp is Error) {
                    resp.apply { error?.overrideErrorCode(ErrorResponse.ERROR_CODE_ADD_USR_CONFIG) }
                }
            }
    }

    private suspend fun uploadImage(
        uploadUrl: String,
        pictureByteArr: ByteArray
    ): DataWrapper<ResponseBody> {

        val req = RequestBody.create(null, pictureByteArr)
        return RequestHandler()
            .apiCall { NetworkProvider.api.uploadImage(uploadUrl, req) }
            .also { resp ->
                if (resp is Error) {
                    resp.apply { error?.overrideErrorCode(ErrorResponse.ERROR_CODE_UPLOAD_PICTURE) }
                }
            }
    }

    suspend fun uploadPicture(pictureByteArr: ByteArray): DataWrapper<ResponseBody> {

        return when (val createUserConfig = createUserConfiguration()) {
            is Success -> {
                when (val addUserConfig =
                    addUserConfiguration(configId = createUserConfig.data.id ?: "")) {
                    is Success -> {
                        uploadImage(
                            uploadUrl = addUserConfig.data.uploadUrl ?: "",
                            pictureByteArr
                        )
                    }
                    is Error -> addUserConfig
                }

            }
            is Error -> createUserConfig
        }
    }

}