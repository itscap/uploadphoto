package com.itscap.networking

import com.google.gson.Gson
import com.itscap.networking.data.DataWrapper
import com.itscap.networking.data.ErrorResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class RequestHandler {

    suspend fun <T> apiCall(
        dispatcher: CoroutineDispatcher = Dispatchers.IO,
        apiCall: suspend () -> T
    ): DataWrapper<T> {
        return withContext(dispatcher) {
            try {
                DataWrapper.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is IOException -> DataWrapper.Error(null, ErrorResponse().asConnection())
                    is HttpException -> DataWrapper.Error(
                        throwable.code(),
                        buildError(throwable)
                    )
                    else -> DataWrapper.Error(null, ErrorResponse().asGeneric())
                }
            }
        }
    }

    private fun buildError(throwable: HttpException): ErrorResponse? {
        return try {
            throwable.response()?.errorBody()?.string()?.let { errorRespBodyStr ->
                Gson().fromJson(errorRespBodyStr, ErrorResponse::class.java)
            }
        } catch (exception: Exception) {
            null
        }
    }

}