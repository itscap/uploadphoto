package com.itscap.uploadphoto.customviews

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.itscap.uploadphoto.R


class Alert : DialogFragment() {

    companion object {
        private const val TAG = "ALERT_TAG"
    }

    private var titleStr: String? = null
    private var srcImageId: Int? = null
    private var descStr: SpannableString? = null
    private var buttonPositiveTitle: String? = null
    private var buttonNegativeTitle: String? = null
    private var cancelableOnTouchOutside = false
    private var onDismissListener: () -> (Unit) = { }
    private var buttonPositiveListener: () -> (Unit) = { }
    private var buttonNegativeListener: () -> (Unit) = { }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val vDialog = super.onCreateDialog(savedInstanceState)
        vDialog.setCancelable(cancelableOnTouchOutside)
        vDialog.setCanceledOnTouchOutside(cancelableOnTouchOutside)
        isCancelable = cancelableOnTouchOutside
        return vDialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val params =
                if (isFullscreen()) WindowManager.LayoutParams.MATCH_PARENT else WindowManager.LayoutParams.WRAP_CONTENT
            it.setLayout(params, params)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return setupUI(inflateViewRes(inflater, container))
    }

    fun isFullscreen(): Boolean {
        return false
    }

    fun inflateViewRes(inflater: LayoutInflater, container: ViewGroup?): View {
        return inflater.inflate(R.layout.alert_dialog, container, false)
    }

    private fun setupUI(view: View): View {

        setupTitle(view)
        setupDesc(view)
        setupButtonPositive(view)
        setupButtonNegative(view)

        return view
    }

    fun setupTitle(view: View) {
        titleStr?.let {
            (view.findViewById(R.id.tv_title) as TextView).apply {
                text = it
                visibility = View.VISIBLE
            }
        }
    }

    fun setupDesc(view: View) {
        descStr?.let { (view.findViewById(R.id.tv_desc) as TextView).text = it }
    }

    fun setupButtonPositive(view: View) {

        buttonPositiveTitle?.let { title ->
            (view.findViewById<Button>(R.id.button_positive))?.apply {
                text = title
                visibility = View.VISIBLE
                setOnClickListener {
                    buttonPositiveListener()
                    dismiss()
                }
            }
        }
    }

    fun setupButtonNegative(view: View) {

        buttonNegativeTitle?.let { title ->
            (view.findViewById<Button>(R.id.button_negative))?.apply {
                setText(title)
                visibility = View.VISIBLE
                setOnClickListener {
                    buttonNegativeListener()
                    dismiss()
                }
            }
        }
    }

    override fun dismiss() {
        try {
            super.dismiss()
        } catch (e: Exception) {

        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener()
    }

    fun dismissOnTouchOutside(dismissOnTouchOutside: Boolean): Alert {
        cancelableOnTouchOutside = dismissOnTouchOutside
        return this
    }

    fun onDismissListener(onDismiss: () -> (Unit)): Alert {
        onDismissListener = onDismiss
        return this
    }

    fun andButtonPositive(title: String, buttonPositiveListener: () -> (Unit)): Alert {
        this.buttonPositiveTitle = title
        this.buttonPositiveListener = buttonPositiveListener
        return this
    }

    fun andButtonNegative(title: String, buttonNegativeListener: () -> (Unit)): Alert {
        this.buttonNegativeTitle = title
        this.buttonNegativeListener = buttonNegativeListener
        return this
    }

    fun withTitle(title: String?): Alert {
        titleStr = title
        return this
    }

    fun withImage(srcImage: Int): Alert {
        srcImageId = srcImage
        return this
    }

    fun withDesc(desc: String?): Alert {
        descStr = SpannableString(desc)
        return this
    }

    fun withDesc(descSpannable: SpannableString): Alert {
        descStr = descSpannable
        return this
    }

    fun show(activity: AppCompatActivity?) {
        try {
            activity?.let {
                if (!it.supportFragmentManager.isStateSaved && !it.isFinishing) {
                    show(it.supportFragmentManager, TAG)
                }
            }
        } catch (ignored: IllegalStateException) {
        } catch (ignored: WindowManager.BadTokenException) {
        }
    }

    fun show(fragment: Fragment?) {
        try {
            fragment?.let {
                if (!it.childFragmentManager.isStateSaved && !it.isRemoving) {
                    show(it.childFragmentManager, TAG)
                }
            }
        } catch (ignored: IllegalStateException) {
        } catch (ignored: WindowManager.BadTokenException) {
        }
    }
}