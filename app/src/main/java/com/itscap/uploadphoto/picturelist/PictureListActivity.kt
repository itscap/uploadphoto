package com.itscap.uploadphoto.picturelist

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.itscap.data.PictureAcquisitionHelper
import com.itscap.networking.data.DataWrapper
import com.itscap.networking.data.ErrorResponse
import com.itscap.networking.data.ErrorResponse.Companion.ERROR_CODE_RETRIEVE_PICTURES
import com.itscap.uploadphoto.R
import com.itscap.uploadphoto.customviews.Alert
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.loading_dialog_layout.*

class PictureListActivity : AppCompatActivity() {

    private val viewModel by lazy { ViewModelProvider(this).get(PictureListViewModel::class.java) }
    private val picturesAdapter by lazy { PicturesAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.loadingLiveData.observe(this) { isLoading ->
            loading_dialog.visibility = if (isLoading) View.VISIBLE else View.GONE
        }
        viewModel.picturesListLiveData.observe(this) { pictures ->
            picturesAdapter.setList(pictures)
        }

        rv_pictures.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = picturesAdapter.onUploadButtonClicked { picture ->
                uploadPicture(this.context, picture)
            }
        }

        btn_choose_photo.setOnClickListener {
            loadPictures()
        }

        if (viewModel.picturesListLiveData.value == null) {
            Alert()
                .withDesc(getString(R.string.choose_some_pictures))
                .andButtonPositive(getString(R.string.ok)) {
                    loadPictures()
                }
                .show(this)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PictureAcquisitionHelper.REQ_COD_INTENT_REQ_STORAGE_PERMISSIONS -> {
                if (!grantResults.contains(PackageManager.PERMISSION_DENIED)) {
                    loadPictures()
                } else {
                    Alert()
                        .withDesc(
                            ErrorResponse(ErrorResponse.ERROR_CODE_STORAGE_PERM)
                                .getMessage(this)
                        )
                        .andButtonPositive(getString(R.string.ok)) { }
                        .show(this)
                }
            }
            else -> null
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            PictureAcquisitionHelper.REQ_COD_INTENT_CHOOSE_IMAGES -> {
                if (resultCode == RESULT_OK) {
                    data?.clipData
                        ?.let {
                            viewModel.getBitmapList(this, it).observe(this) { errorResp ->
                                onRetrievePicturesError(errorResp)
                            }
                        }
                        ?: run { onRetrievePicturesError(ErrorResponse(ERROR_CODE_RETRIEVE_PICTURES)) }
                }
            }
            else -> null
        }
    }

    private fun loadPictures() {
        if (viewModel.hasStoragePermissions(this)) {
            viewModel.startRetrievePicturesIntent(this)
        } else {
            viewModel.requireStoragePermissions(this)
        }
    }

    private fun uploadPicture(context: Context, picture: Bitmap) {
        viewModel.uploadPicture(context, picture).observe(this) { dataWrapper ->
            when (dataWrapper) {
                is DataWrapper.Success -> {
                    Alert()
                        .withDesc(getString(R.string.upload_successfully))
                        .andButtonPositive(getString(R.string.ok)) { }
                        .show(this)
                }
                is DataWrapper.Error -> {
                    Alert()
                        .withDesc(dataWrapper.error?.getMessage(this))
                        .andButtonPositive(getString(R.string.retry)) {
                            uploadPicture(context, picture)
                        }
                        .andButtonNegative(getString(R.string.ok)) { }
                        .show(this)
                }
            }
        }
    }

    private fun onRetrievePicturesError(error: ErrorResponse) {
        Alert()
            .withDesc(error.getMessage(this))
            .andButtonPositive(getString(R.string.retry)) {
                loadPictures()
            }
            .andButtonNegative(getString(R.string.ok)) { }
            .show(this)
    }

}