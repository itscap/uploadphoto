package com.itscap.uploadphoto.picturelist

import android.app.Application
import android.content.ClipData
import android.content.Context
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.itscap.data.PictureAcquisitionHelper
import com.itscap.data.PictureCompressor
import com.itscap.networking.RequestManager
import com.itscap.networking.data.DataWrapper
import com.itscap.networking.data.ErrorResponse
import com.itscap.networking.data.ErrorResponse.Companion.ERROR_CODE_RETRIEVE_PICTURES
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.ResponseBody

class PictureListViewModel(app: Application) : AndroidViewModel(app) {

    private val pictureAcquisitionHelper by lazy { PictureAcquisitionHelper() }
    var loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    var picturesListLiveData: MutableLiveData<List<Bitmap>> = MutableLiveData()

    private suspend fun getCompressedPictureByteArr(context: Context, picture: Bitmap): ByteArray? {
        var byteArr: ByteArray? = null
        val compressor = PictureCompressor()
        val compressedPic = compressor.compressAndScalePicture(
            context = context,
            bitmap = picture,
            quality = 50,
            maxHeight = 10f,
            maxWidth = 10f
        )
        if (compressedPic != null) {
            byteArr = compressor.convertBitmapToByteArray(compressedPic)
        }
        return byteArr
    }

    fun hasStoragePermissions(context: Context) =
        pictureAcquisitionHelper.hasStoragePermissions(context)

    fun requireStoragePermissions(activity: AppCompatActivity) =
        pictureAcquisitionHelper.requireStoragePermissions(activity)


    fun startRetrievePicturesIntent(activity: AppCompatActivity) {
        pictureAcquisitionHelper.startRetrievePicturesIntent(activity)
    }

    fun getBitmapList(activity: AppCompatActivity, clipData: ClipData): LiveData<ErrorResponse> {
        loadingLiveData.postValue(true)
        val errLiveData = MutableLiveData<ErrorResponse>()
        viewModelScope.launch(Dispatchers.IO) {
            val picturesList = pictureAcquisitionHelper.getBitmapListFromActivityResults(
                activity.contentResolver,
                clipData
            )
            if (picturesList.isNullOrEmpty()) {
                errLiveData.postValue(ErrorResponse(ERROR_CODE_RETRIEVE_PICTURES))
            } else {
                picturesListLiveData.postValue(picturesList)
            }
            loadingLiveData.postValue(false)
        }
        return errLiveData
    }

    fun uploadPicture(context: Context, picture: Bitmap): LiveData<DataWrapper<ResponseBody>> {
        loadingLiveData.postValue(true)
        val liveData = MutableLiveData<DataWrapper<ResponseBody>>()

        viewModelScope.launch(Dispatchers.IO) {
            val compressedPicByteArr = getCompressedPictureByteArr(context, picture)
            if (compressedPicByteArr != null) {
                val userConfigResp = RequestManager().uploadPicture(compressedPicByteArr)
                liveData.postValue(userConfigResp)
            } else {
                liveData.postValue(
                    DataWrapper.Error(
                        null,
                        ErrorResponse(ErrorResponse.ERROR_CODE_COMPRESS_PICTURE)
                    )
                )
            }
            loadingLiveData.postValue(false)
        }
        return liveData
    }

}