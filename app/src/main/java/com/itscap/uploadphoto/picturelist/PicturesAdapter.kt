package com.itscap.uploadphoto.picturelist

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.itscap.uploadphoto.R


class PicturesAdapter : RecyclerView.Adapter<PicturesAdapter.PicturesViewHolder>() {

    private var picturesList: MutableList<Bitmap> = mutableListOf()
    private var onUploadButtonClicked: (Bitmap) -> Unit = { }

    class PicturesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val ivPicture = itemView.findViewById<ImageView>(R.id.iv_picture)
        private val btnUpload = itemView.findViewById<TextView>(R.id.btn_upload)

        fun bind(picture: Bitmap, onButtonClicked: (Bitmap) -> Unit) {

            Glide.with(ivPicture)
                .load(picture)
                .fallback(R.drawable.ic_launcher_background)
                .into(ivPicture)

            btnUpload.setOnClickListener { onButtonClicked(picture) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PicturesViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.picture_row, parent, false)
    )

    override fun getItemCount() = picturesList.size

    override fun onBindViewHolder(holder: PicturesViewHolder, position: Int) =
        holder.bind(picturesList[position], onUploadButtonClicked)

    fun onUploadButtonClicked(onUploadButtonClicked: (Bitmap) -> Unit): PicturesAdapter {
        this.onUploadButtonClicked = onUploadButtonClicked
        return this
    }

    fun setList(list: List<Bitmap>) {
        picturesList.clear()
        picturesList.addAll(list)
        notifyDataSetChanged()
    }
}