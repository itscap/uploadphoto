package com.itscap.uploadphoto

import android.app.Application
import com.itscap.networking.NetworkProvider

class UploadPhotoApp : Application() {

    override fun onCreate() {
        super.onCreate()

        NetworkProvider.setup()
    }
}